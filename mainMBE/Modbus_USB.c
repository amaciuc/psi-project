#include "Modbus_USB.h"
#include <stdio.h>
#include "cmsis_os.h"

char array_USB[20];

// Definition of Thread_modbus_USB
void Thread_modbus_USB (void const *arg)
{
	sprintf(array_USB, "%s", "modbus_USB");
}

// Access to the thread definition
osThreadDef (Thread_modbus_USB, osPriorityNormal, 1, 0);            

// Create thread
void ThreadCreate_modbus_USB (void) {
  osThreadId id;
	// create the thread
  id = osThreadCreate (osThread (Thread_modbus_USB), NULL); 
	// handle thread creation	
  if (id == NULL) {                                        
    // Failed to create a thread
  }
	// stop the thread
  osThreadTerminate (id);                                  
}
