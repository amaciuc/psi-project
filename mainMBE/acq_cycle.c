#include "acq_cycle.h"
#include <stdio.h>
#include "cmsis_os.h"

char array_acq[20];

// Definition of Thread_acq_cycle
void Thread_acq_cycle (void const *arg)
{
	sprintf(array_acq, "%s", "acq_cycle");
}

// Access to the thread definition
osThreadDef (Thread_acq_cycle, osPriorityNormal, 1, 0);            

// Create thread
void ThreadCreate_acq_cycle (void) {
  osThreadId id;
	// create the thread
  id = osThreadCreate (osThread (Thread_acq_cycle), NULL); 
	// handle thread creation	
  if (id == NULL) {                                        
    // Failed to create a thread
  }
	// stop the thread
  osThreadTerminate (id);                                  
}
