#include "Modbus_socket.h"
#include <stdio.h>
#include "cmsis_os.h"

char array_socket[20];

// Definition of Thread_modbus_socket
void Thread_modbus_socket (void const *arg)
{
	sprintf(array_socket, "%s", "modbus_socket");
}

// Access to the thread definition
osThreadDef (Thread_modbus_socket, osPriorityNormal, 1, 0);            

// Create thread
void ThreadCreate_modbus_socket (void) {
  osThreadId id;
	// create the thread
  id = osThreadCreate (osThread (Thread_modbus_socket), NULL); 
	// handle thread creation	
  if (id == NULL) {                                        
    // Failed to create a thread
  }
	// stop the thread
  osThreadTerminate (id);                                  
}
