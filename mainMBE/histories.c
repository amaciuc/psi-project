#include "histories.h"
#include <stdio.h>
#include "cmsis_os.h"

char array_histories[20];

// Definition of Thread_histories
void Thread_histories (void const *arg)
{
	sprintf(array_histories, "%s", "histories");
}

// Access to the thread definition
osThreadDef (Thread_histories, osPriorityNormal, 1, 0);            

// Create thread
void ThreadCreate_histories (void) {
  osThreadId id;
	// create the thread
  id = osThreadCreate (osThread (Thread_histories), NULL); 
	// handle thread creation	
  if (id == NULL) {                                        
    // Failed to create a thread
  }
	// stop the thread
  osThreadTerminate (id);                                  
}